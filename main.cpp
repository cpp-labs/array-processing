#include <iostream>

enum StudentNum {
    Oleksandr = 0,
    Nick = 1,
    Vladislav = 2,
    Stanislav = 3,
    Polina = 4,
};

// Adding score to student and returns total score
int addScore(int scores[], StudentNum student, int value) {
    scores[student] += value;
    return scores[student];
}

int getRandomInt (int min = 0, int max = 100) {
    return rand() % (max - min) + min;
}

int main() {
    int maxStudents = 5;

    int scores[maxStudents];

    // Filling scores array
    for (int i = 0; i < maxStudents; i++) {
        scores[i] = getRandomInt();
    }

    // Show array results
    std::cout << "Oleksandr's score = " << scores[StudentNum::Oleksandr] << std::endl;
    std::cout << "Nick's score = " << scores[StudentNum::Nick] << std::endl;
    std::cout << "Vladislav's score = " << scores[StudentNum::Vladislav] << std::endl;
    std::cout << "Stanislav's score = " << scores[StudentNum::Stanislav] << std::endl;
    std::cout << "Polina's score = " << scores[StudentNum::Polina] << std::endl;

    // Add 5 scores to Stanislav
    addScore(scores, StudentNum::Stanislav, 5);
    std::cout << std::endl << "Updated scores for Stanislav: " << scores[StudentNum::Stanislav] << std::endl;

    // Add another 10
    addScore(scores, StudentNum::Stanislav, 10);
    std::cout << std::endl << "Updated scores for Stanislav: " << scores[StudentNum::Stanislav] << std::endl;

    return 0;
}
